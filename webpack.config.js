module.exports = {
    entry: "./src/js/main.js",
    output: {
        path: __dirname + "/app/js",
        filename: "build.js"
    },

    watch: true,

    watchOptions: {
    	aggregateTimeout: 100
    }
};