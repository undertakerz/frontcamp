'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var News = function () {
    function News(newsItem) {
        _classCallCheck(this, News);

        for (var key in newsItem) {
            var val = newsItem[key];
            this[key] = val;
        }
    }

    _createClass(News, [{
        key: "createNews",
        value: function createNews() {
            var newsDiv = document.createElement("div"),
                newsTitle = document.createElement("a"),
                newsAnnotation = document.createElement("p"),
                newsImg = document.createElement("img");

            newsDiv.className = "news";

            var newsContainer = document.getElementById("content").appendChild(newsDiv);
            newsContainer.appendChild(newsImg);
            newsContainer.appendChild(newsTitle);
            newsContainer.appendChild(newsAnnotation);

            for (var key in this) {
                var val = this[key];
                if (key === "title") {
                    newsTitle.innerHTML = newsTitle.innerHTML + ("" + val);
                }
                if (key === "url") {
                    newsTitle.href = newsTitle.href + ("" + val);
                }
                if (key === "abstract") {
                    newsAnnotation.innerHTML = newsAnnotation.innerHTML + ("" + val);
                }
                if (key === "thumbnail_standard") {
                    newsImg.src = newsImg.src + ("" + val);
                }
            }
        }
    }]);

    return News;
}();

// Request


fetch("http://api.nytimes.com/svc/news/v3/content/all/all/1.json?api-key=b73ca49f8555d9d00351c678056783b7:14:74981220").then(function (response) {
    return response.json();
}).then(function (json) {
    loadResponse(json);
}).catch(function (err) {
    console.log(err);
});

function loadResponse(response) {
    var newsArray = response.results;

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = newsArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var newsItem = _step.value;

            //console.log(newsItem);
            var news = new News(newsItem);
            news.createNews();
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }
}
