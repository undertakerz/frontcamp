'use strict';

class News {
    constructor(newsItem) {
        for (let key in newsItem) {
            let val = newsItem[key];
            this[key] = val;
        }
    }

    createNews(){
        let newsDiv = document.createElement("div"),
            newsTitle = document.createElement("a"),
            newsAnnotation = document.createElement("p"),
            newsImg = document.createElement("img");

        newsDiv.className = "news";

        let newsContainer = document.getElementById("content").appendChild(newsDiv);
        newsContainer.appendChild(newsImg);
        newsContainer.appendChild(newsTitle);
        newsContainer.appendChild(newsAnnotation);

        for (let key in this) {
            let val = this[key];
            if (key === "title") {
                newsTitle.innerHTML = newsTitle.innerHTML + `${val}`;
            }
            if (key === "url") {
                newsTitle.href = newsTitle.href + `${val}`;
            }
            if (key === "abstract") {
                newsAnnotation.innerHTML = newsAnnotation.innerHTML + `${val}`;
            }
            if (key === "thumbnail_standard") {
                newsImg.src = newsImg.src + `${val}`;
            }

        }
    }
}

// Request
fetch("http://api.nytimes.com/svc/news/v3/content/all/all/1.json?api-key=b73ca49f8555d9d00351c678056783b7:14:74981220")
    .then(function(response) {
        return response.json();
    })
    .then(function(json) {
        loadResponse(json);
    })
.catch(function(err) {
    console.log(err);
});

function loadResponse(response) {
    let newsArray = response.results;

    for (let newsItem of newsArray) {
        //console.log(newsItem);
        let news = new News(newsItem);
        news.createNews();
    }
}