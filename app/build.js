/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';

	class News {
	    constructor(newsItem) {
	        for (let key in newsItem) {
	            let val = newsItem[key];
	            this[key] = val;
	        }
	    }

	    createNews(){
	        let newsDiv = document.createElement("div"),
	            newsTitle = document.createElement("a"),
	            newsAnnotation = document.createElement("p"),
	            newsImg = document.createElement("img");

	        newsDiv.className = "news";

	        let newsContainer = document.getElementById("content").appendChild(newsDiv);
	        newsContainer.appendChild(newsImg);
	        newsContainer.appendChild(newsTitle);
	        newsContainer.appendChild(newsAnnotation);

	        for (let key in this) {
	            let val = this[key];
	            if (key === "title") {
	                newsTitle.innerHTML = newsTitle.innerHTML + `${val}`;
	            }
	            if (key === "url") {
	                newsTitle.href = newsTitle.href + `${val}`;
	            }
	            if (key === "abstract") {
	                newsAnnotation.innerHTML = newsAnnotation.innerHTML + `${val}`;
	            }
	            if (key === "thumbnail_standard") {
	                newsImg.src = newsImg.src + `${val}`;
	            }

	        }
	    }
	}

	// Request
	fetch("http://api.nytimes.com/svc/news/v3/content/all/all/1.json?api-key=b73ca49f8555d9d00351c678056783b7:14:74981220")
	    .then(function(response) {
	        return response.json();
	    })
	    .then(function(json) {
	        loadResponse(json);
	    })
	.catch(function(err) {
	    console.log(err);
	});

	function loadResponse(response) {
	    let newsArray = response.results;

	    for (let newsItem of newsArray) {
	        //console.log(newsItem);
	        let news = new News(newsItem);
	        news.createNews();
	    }
	}

/***/ }
/******/ ]);